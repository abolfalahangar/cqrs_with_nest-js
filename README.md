[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

- This project does not have a database, just a log

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
