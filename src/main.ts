import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const documentOptions = new DocumentBuilder()
    .setTitle('CQRS NESTjs')
    .setDescription('CQRS NESTjs')
    .setVersion('1.0.0')
    .setBasePath(`/`)
    .build();
  const document = SwaggerModule.createDocument(app, documentOptions);

  SwaggerModule.setup('/api', app, document);
  await app.listen(3000);
}
bootstrap();
