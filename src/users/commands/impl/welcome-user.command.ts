import { ICommand } from '@nestjs/cqrs';
import { UserDto } from '../../dtos/users.dto';

export class WelcomeUserCommand implements ICommand {
  constructor(public readonly userDto: UserDto) {}
}
