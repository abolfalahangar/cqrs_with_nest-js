import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserIdRequestParamsDto {
  @IsString()
  @ApiProperty()
  readonly userId!: string;
}

export class UserDto {
  @IsString()
  @ApiProperty()
  readonly userId!: string;
  @IsString()
  @ApiProperty()
  readonly firstName!: string;
  @IsString()
  @ApiProperty()
  readonly lastName!: string;
}
