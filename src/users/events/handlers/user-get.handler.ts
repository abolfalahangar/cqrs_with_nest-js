import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { UserGetEvent } from '../impl/user.get.event';
import { Logger } from '@nestjs/common';

@EventsHandler(UserGetEvent)
export class UserGetHandler implements IEventHandler<UserGetEvent> {
  handle(event: UserGetEvent) {
    Logger.log(event, 'UserGetEvent'); // write here
  }
}
