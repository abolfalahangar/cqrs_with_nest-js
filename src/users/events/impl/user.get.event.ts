import { IEvent } from '@nestjs/cqrs';

export class UserGetEvent implements IEvent {
  constructor(public readonly userId: string) {}
}
