import { IQueryHandler, QueryHandler, EventPublisher } from '@nestjs/cqrs';
import { UserRepository } from '../../repository/user.repository';
import { UserGetQuery } from '../impl/user.get.query';
import { Logger } from '@nestjs/common';

@QueryHandler(UserGetQuery)
export class UserGetHandler implements IQueryHandler<UserGetQuery> {
  constructor(
    private readonly repository: UserRepository,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(query: UserGetQuery) {
    Logger.log('Async UserGetHandler...', 'UserGetQuery');
    const { userIdRequestParamsDto } = query;
    const user = this.publisher.mergeObjectContext(
      await this.repository.getUser(userIdRequestParamsDto),
    );
    user.commit();
  }
}
