import { IQuery } from '@nestjs/cqrs';
import { UserIdRequestParamsDto } from '../../dtos/users.dto';

export class UserGetQuery implements IQuery {
  constructor(public readonly userIdRequestParamsDto: UserIdRequestParamsDto) {}
}
