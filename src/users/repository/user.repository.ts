import { Injectable } from '@nestjs/common';
import { User } from '../models/user.model';
import { UserDto, UserIdRequestParamsDto } from '../dtos/users.dto';
@Injectable()
export class UserRepository {
  async createUser(userDto: UserDto) {
    const user = new User(undefined);
    user.setData(userDto);
    user.createUser();
    return user;
  }

  async updateUser(userDto: UserDto) {
    const user = new User(userDto.userId);
    user.setData(userDto);
    user.updateUser();
    return user;
  }

  async deleteUser(userIdRequestParamsDto: UserIdRequestParamsDto) {
    const user = new User(userIdRequestParamsDto.userId);
    user.deleteUser();
    return user;
  }

  async welcomeUser(userDto: UserDto) {
    const user = new User(userDto.userId);
    user.welcomeUser();
    return user;
  }
  async getUser(userIdRequestParamsDto: UserIdRequestParamsDto) {
    const user = new User(userIdRequestParamsDto.userId);
    user.getUser();
    return user;
  }
}
