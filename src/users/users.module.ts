import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CommandHandlers } from './commands/handlers';
import { QueryHandlers } from './queries/handlers';
import { EventHandlers } from './events/handlers';
import { UsersController } from './controllers/users.controller';
import { UserRepository } from './repository/user.repository';
import { UsersService } from './services/users.service';
import { UsersSagas } from './sagas/users.sagas';

@Module({
  imports: [CqrsModule],
  controllers: [UsersController],
  providers: [
    UserRepository,
    ...CommandHandlers,
    ...EventHandlers,
    ...QueryHandlers,
    UsersSagas,
    UsersService,
  ],
})
export class UsersModule {}
